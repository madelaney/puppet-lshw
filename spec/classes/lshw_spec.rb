require 'spec_helper'

describe 'lshw' do
  on_supported_os.each do |os, os_facts|
    context "on #{os} with defaults" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
    end

    ['absent', 'present', '1.0.0'].each do |arg|
      context "on #{os} with #{arg}" do
        let(:facts) { os_facts }
        let(:params) { { 'ensure' => arg } }

        it { is_expected.to compile }
      end
    end
  end
end
