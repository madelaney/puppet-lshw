class lshw(
  Variant[Enum['present','absent'], String] $ensure = present
) {
  package {
    'lshw':
      ensure => $ensure;
  }
}
